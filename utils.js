/**
 * Common utility helper methods used by E2E/BVT tests
 */

var protractorSharedUtils = function () {
    'use strict';


    var username                = element.all(by.model('user.UserName')).first(),
        password                = element.all(by.model('model')).first(),
        submit_button           = element.all(by.css('#login_form input[type="submit"]')).first(),
        signout_button          = element(by.css('[translate="SIGN_OUT"]')),
        demo_button             = element(by.id('demoButton')),
        demo_01Login            = element(by.xpath('//*[@id="demoOptions"]/p[1]/a'));

    var monitor = "https://monitor.us.sunpower.com"
    var devcp = "https://devcp.us.sunpowermonitor.com";
    var vendorcp = "https://vendorcp.us.sunpowermonitor.com";
    var qacp = "https://qacp.us.sunpowermonitor.com";
    this.loginUser = function(user, pword) {
        browser.get(devcp+'/#/signout').then(function(){
        username.sendKeys(user);
        password.sendKeys(pword);
        submit_button.click();
      });
    };

    this.demoLogin = function(){
      browser.get(monitor+'/#/signout').then(function(){
        browser.sleep(3000);
        demo_button.click();
        demo_01Login.click();
      });
    };

    this.goToPage = function(page){
      browser.get(qacp+'/#/'+page);
    };

    this.signout = function(){
        browser.executeScript('window.localStorage.clear();');
    };
// TODO : update waitForElement with msg and .then
    this.waitForElement = function(el){
      browser.wait(function(){
        return el.isDisplayed();
      }, 8000);
    };

    this.waitForText = function(el) {
        return browser.wait(function () {
             return el.getText().then(function(text){
                 return text.length > 0;
             });
        }, 3000);
    };

    this.waitForPage = function(name){
        return browser.wait(function(){
            return browser.getCurrentUrl().then(function(url){
                return url.indexOf(name) > -1;
            });
        }, 50000);
    };

    this.testExternalLink = function(link, expected_url){
        return link.click().then(function () {
            return browser.getAllWindowHandles().then(function (handles) {
                return browser.switchTo().window(handles[1]).then(function () {
                    return browser.wait(function(){
                        return browser.driver.getCurrentUrl().then(
                            function onSuccess(url){
                                if (url === expected_url){
                                    return browser.driver.close().then(function () {
                                        //to switch to the previous window
                                        browser.switchTo().window(handles[0]);
                                        return true;
                                    });
                                }
                                return false;
                            }
                        );
                    }, 3000);
                });
            });
        });
    };

    this.stripTags = function(str){
        return str.replace(/<\/?[^>]+(>|$)/g, "");
    };

    this.keyValuePair = function(_e){
      return {
        isDisplayed: _e.isDisplayed(),
        key: _e.getText().then(function(str){
          var array = str.split(':');
          return array[0].trim();
        }),
        value: _e.getText().then(function(str){
          var array = str.split(':');
          return array[1].trim();
        })
      }
    };

    this.click = function(e){
      e.click();
    };

};

var _retryOnError = function(error){
  browser.sleep(500);
  return false;
};
module.exports = new protractorSharedUtils();
