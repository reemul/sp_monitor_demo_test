'use-strict'
/**
  * Created by Reemul Uzamaki 08/26/2016
  */
module.exports = [
  { user: "eis11@test.com", password: "eispwd11" },
  { user: "eis12@test.com", password: "eispwd12" },
  { user: "eis13@test.com", password: "eispwd13" },
  { user: "eis14@test.com", password: "eispwd14" },
  { user: "eis15@test.com", password: "eispwd15" }
];
