'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */

var gulp = require('gulp'),
    protractor = require('gulp-protractor').protractor,
    webdriverUpdate = require('gulp-protractor').webdriver_update,
    webdriver = require('gulp-protractor').webdriver_standalone,
    jasmine = require('gulp-jasmine'),
    reporters = require('jasmine-reporters');

require('gulp-require-tasks')();
