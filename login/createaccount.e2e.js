'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var LoginPO = require('./po/login.po.js');
var RegistrationPO = require('./po/registration.po.js');
var _BASE_URL = 'https://vendorcp.us.sunpowermonitor.com/#/signout';

describe('Login page', function(){
var util = require('../utils');
var loginPO = new LoginPO();
var registrationPO = new RegistrationPO();
var _HEADER_ = 'Enter Account Information';
// Sign in page UI elements validation
  describe('Create new account.', function() {

    beforeAll(function(){
      browser.get(_BASE_URL);
    });

    afterAll(function(){
      browser.get(_BASE_URL);
    });

    it('should show the page title in the browser as SunPower', function() {
      expect(browser.getTitle()).toEqual('SunPower');
    });

    it('should display "Sign In" as the page header', function(){
      util.waitForPage('/login');
      expect(loginPO.header()).toEqual('Sign In');
    });

    it('should display the Create New Account link', function() {
      expect(loginPO.createAccount().isDisplayed).toBeTruthy();
    });

    it('should display the Create New Account link text as Create account', function(){
      expect(loginPO.createAccount().text).toEqual('Create account');
    });

    it('should navigate to the registration page when clicking Create account', function(){
      loginPO.createAccount().click();
      util.waitForPage('register');
      browser.getCurrentUrl().then(
          function onSuccess(url){
              expect(url.indexOf('register/enter_email') > -1).toBe(true);
          }
      );
    });

    it('should display page header as "Enter Account Information"', function(){
      expect(registrationPO.header()).toEqual(_HEADER_);
    });

    it('should submit Email and confirmation Email information', function(){
      expect(registrationPO.setEmail()).toBeTruthy(); // TODO : needs validation
    });

  }); // end describe
});
