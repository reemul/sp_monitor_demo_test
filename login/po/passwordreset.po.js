'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var Page = require('protractor-pageobject').Page;

var PasswordResetPO = Page.extend({
  els: {
    header: by.xpath('//header/h3'),
    reset_form: by.id('re_form')
  },
  comps: {
    reset_form: require('../co/resetpasswordform.co.js')
  },

  setEmail: function(email){
    this.component('reset_form').next(email);
  },

  header: function(){
    this.waitForElement('header', 3000);
    var _e = this.element('header');
    return _e.getText();
  }
});
module.exports = PasswordResetPO;
