'use-strict';
/**
 * Created by Reemul Uzamaki 09/14/2016
 */
var Page = require('protractor-pageobject').Page;
var LoginPage = Page.extend({
  els:{
    header: by.xpath('//header/h3'),
    // TODO : Need to clean up these selectors !!!!
    forgot_pwd: by.xpath('//*[@id="container"]/main/div[1]/div[2]/article/div/div/div[1]/a[1]'),
    create_acct: by.xpath('//*[@id="container"]/main/div[1]/div[2]/article/div/div/div[1]/a[2]'),
    demo_btn: by.id('demoButton'),
    demo_ops: by.id('demoOptions'),
    help_btn: by.css('.sp-help'),
    signin_form: by.id('login_form')
  },

  comps:{
    signin_form: {
      model: require('loginform.co')
    }
  },

  header: function(){
    this.waitForElement('header', 3000);
    var _e = this.element('header');
    return _e.getText();
  },

  signInForm: function(){
    this.waitForElement('signin_form', 3000);
    var form = this.component('signin_form');
    return form;
  },

  title: function(){
    this.waitForElement('title', 5000);
    var _e = this.element('title');
      return _e.getText();
  },

  forgotPassword: function(){
    this.waitForElement('forgot_pwd', 5000);
    var _e = this.element('forgot_pwd');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText(),
      click: function(){
        _e.click();
      }
    }
  },

  createAccount: function(){
    this.waitForElement('create_acct');
    var _e = this.element('create_acct');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText(),
      click: function(){
        _e.click();
      }
    }
  },

  demoOptions: function(){
    return {
      isDisplayed: this.element('demo_ops').isDisplayed()
    }
  },

  helpButton: function(){
    this.waitForElement('help_btn', 3000);
    return this.isElementPresent('help_btn');
  }

});
module.exports = LoginPage;
