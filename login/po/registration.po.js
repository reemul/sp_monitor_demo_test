'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var Page = require('protractor-pageobject').Page;
var RegistrationPO = Page.extend({

  els: {
    header: by.xpath('//header/h3'),
    registration_form: by.id('re_form')
  },

  comps: {
    registration_form: require('../co/registrationform.co.js')
  },

  setEmail: function(){
    this.component('registration_form').submitEmail('theHulk@test.com');
    return true;
  },

  header: function(){
    this.waitForElement('header', 3000);
    var _e = this.element('header');
    return _e.getText();
  }

});

module.exports = RegistrationPO;
