'use-strict';
/**
 * Created by Reemul Uzamaki 09/14/2016
 */
var Page = require('protractor-pageobject').Page;
var WebmailLoginPage = Page.extend({
  els:{
    header: by.id('header'),
    userInput: by.id('userNameInput'),
    pwdInput: by.id('passwordInput'),
    submitButton: by.id('submitButton')
  },
  comps:{  },

  header: function(){
    this.waitForElement('header', 3000);
    var _e = this.element('header');
    return _e.getText();
  },

  title: function(){
    this.waitForElement('title', 5000);
    var _e = this.element('title');
      return _e.getText();
  },

  login: function(usr, pwd){
    var email = this.element('userInput');
    var pword = this.element('pwdInput');
    email.sendKeys(usr);
    pword.sendKeys(pwd);
    this.element('submitButton').click();

    return true;
  }

});
module.exports = WebmailLoginPage;
