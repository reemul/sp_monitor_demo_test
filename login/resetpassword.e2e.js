'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var LoginPO = require('login.po.js');
var PasswordResetPO = require('./po/passwordreset.po.js');
var _BASE_URL = 'https://devcp.us.sunpowermonitor.com/#';

describe('Login page', function(){
var util = require('../utils');

var loginPO = new LoginPO();
var passwordResetPO = new PasswordResetPO();
var LOGIN_HEADER = 'Enter Account Information';
var PASSWORD_RESET_HEADER = 'Reset Your Password';
var FORGOT_PASSWORD_TEXT = 'Forgot password?';

// Sign in page UI elements validation
  describe('Reset password.', function() {

    beforeAll(function(){
      browser.get(_BASE_URL);
    });

    afterAll(function(){
      browser.get(_BASE_URL);
    });

    it('should show the page title in the browser as SunPower', function() {
      expect(browser.getTitle()).toEqual('SunPower');
    });

    it('should display "Sign In" as the page header', function(){
      util.waitForPage('/login');
      expect(loginPO.header()).toEqual('Sign In');
    });

    it('should display the Forgot password? link', function(){
      expect(loginPO.forgotPassword().isDisplayed).toBeTruthy();
    });

    it('should navigate to the Reset Your Password Password page', function(){
      loginPO.forgotPassword().click();
      util.waitForPage('resetpassword').then(function(){
        browser.getCurrentUrl().then(function onSuccess(url){
          expect(url.indexOf('/resetpassword') > -1).toEqual(true);
        });
      });
    });

    it('should display the page header as "Reset Your Password"', function(){
      expect(passwordResetPO.header()).toEqual(PASSWORD_RESET_HEADER);
    });

    it('should submit email account for reseting password', function(){
      var email = 'eis7@test.com';
      var RESET_PASSWORD_URL = 'resetpassword/checkemail/'+email;
      passwordResetPO.setEmail(email);
      util.waitForPage(RESET_PASSWORD_URL);
      browser.getCurrentUrl().then(
        function onSuccess(url){
          expect(url.indexOf('/'+RESET_PASSWORD_URL) > -1).toBe(true);
        }
      );
    });
    describe('Email verifiaction', function(){
      var WebmailLoginPO = require('webmaillogin.po.js');
      var webmailLoginPO = new WebmailLoginPO();
      var WEB_MAIL_HEADER = 'adfs.sunpowercorp.com';
      beforeAll(function(){
        // TODO : Login into webmail
        mail_user = 'ruzamaki@sunpowercorp.com';
        mail_pwd = '$7ovo8oR2112';
        mail_url = 'https://adfs.sunpowercorp.com/adfs/ls/?client-request-id=cb18e887-ba7e-4c9c-b10c-e7d94adf8346&username=&wa=wsignin1.0&wtrealm=urn%3afederation%3aMicrosoftOnline&wctx=estsredirect%3d2%26estsrequest%3drQIIAeNisFLJKCkpKLbS188vLcnJz8_Wy09Ly0xO1UvOz9XLL0rPTAGxopiBskVCXALtLyRO1-2a47OR5_lNr_vNbrMYBYpL8wryy1OLkvOLCkBqVzHK4zZSP788Uf8CI-MmJnZfJ8_44GCfE0yXP_PfYhL0L0r3TAkvdktNSS1KLMnMz3vExBtanFrkn5dTGZKfnZo3iZkvJz89My--uCgtPi0nvxwoADSxIDG5JL4kMzk7tWQXs0qagYVpaqp5oq6RmZGJrkmSubGuZZKFoa6hcZp5mkGKeVKKgeEFFoEfLIyLWIHeWVGvK9bvWeq9Nn_5a-FN6XdOser7uxRlpESV5JXnRLh4mBrlFuSUpxq5ZjnrlxsmOZkVZ-VXJLq4-5toh_mX25pYGe7itCXgW_ui1MScXFv0YFJLrUjOsDUEAA2';
      });

      it('should navigate to SunpPower corp webmail and show page title of adfs.sunpowercorp.com', function(){
        browser.get(mail_url);
        util.waitForPage(mail_url);
        browser.getCurrentUrl().then(function onSuccess(){
          expect(webmailLoginPO.header()).toEqual(WEB_MAIL_HEADER);
        });
      });

      it('should successfully login to SunpPower webmail', function(){
        webmailLoginPO.login(mail_user, mail_pwd);
        browser.pause();
      });
    });

  });
});
