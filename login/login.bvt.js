'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var LoginPO = require('./po/login.po.js');
var _BASE_URL = 'https://qacp.us.sunpowermonitor.com/#/signout';

beforeAll(function(){
  browser.get(_BASE_URL);
});

describe('Sign in page validation', function(){

var loginPO = new LoginPO();
// Sign in page UI elements validation
  describe('Verification of Login page elements and components.', function() {

      it('should show the page title in the browser as SunPower', function() {
        expect(browser.getTitle()).toEqual('SunPower');
      });

      it('should display "Sign In" as the page header', function(){
        expect(loginPO.header()).toEqual('Sign In');
      });

      it('should verify the Sign In form is displayed', function(){
        expect(loginPO.signInForm()).toBeTruthy();
      });

      it('should verify the Sign In form Email input is displayed', function(){
        expect(loginPO.signInForm().emailInput().isDisplayed).toBeTruthy();
      });

      it('should verify the Sign In form Password input is displayed', function(){
        expect(loginPO.signInForm().passwordInput().isDisplayed).toBeTruthy();
      });

      it('should verify the Sign In button is displayed', function(){
        expect(loginPO.signInForm().signInBtn().isDisplayed).toBeTruthy();
      });

      it('should show place holder text for email input "email address"', function(){
          expect(loginPO.signInForm().emailInput().placeHolder).toEqual('email address');
      });

      it('should show place holder text for password input as "password"', function(){
          expect(loginPO.signInForm().passwordInput().placeHolder).toEqual('password');
      });

      it('should not display Email input error message', function(){
          expect(loginPO.signInForm().emailInputError().isDisplayed).toBeFalsy();
      });

      it('should not display Password input error message', function(){
          expect(loginPO.signInForm().passwordInputError().isDisplayed).toBeFalsy();
      });

      it('should display the Remember me checkbox', function(){
          expect(loginPO.signInForm().rememberMeChkBox().isDisplayed).toBeTruthy();
      });

      it('should display the Remember me checkbox label', function(){
          expect(loginPO.signInForm().rememberMeChkBoxLabel().isDisplayed).toBeTruthy();
      });

      it('should display the Remember me checkbox label as Remember me', function(){
          expect(loginPO.signInForm().rememberMeChkBoxLabel().text).toEqual('Remember me');
      });

      it('should not display the Demo options', function(){
          expect(loginPO.demoOptions().isDisplayed).toBeFalsy();
      });

      it('should display the Forgot Password link', function() {
          expect(loginPO.forgotPassword().isDisplayed).toBeTruthy();
      });

      it('should display the Create Account link', function() {
          expect(loginPO.createAccount().isDisplayed).toBeTruthy();
      });

      it('should display the Create New Account link text as "Create account"', function(){
          expect(loginPO.createAccount().text).toEqual('Create account');
      });

      it('should display the Help button', function(){
          expect(loginPO.helpButton()).toBeTruthy();
      });
  }); // end describe
}); // end describe
