'use-strict';
/**
 * Created by Reemul Uzamaki 09/16/2016
 */
var Component = require('protractor-pageobject').Component;

var RegistrationForm = Component.extend({
  els: {
    email_input:    by.css('#re_form input[type="email"]'),
    email_confirm:  by.css('#re_form input[name="email_confirm"]'),
    next_button:    by.css('#re_form input[type="submit"]')
  },
  comps: {

  },

  submitEmail: function(email){
    this.element('email_input').sendKeys(email);
    this.element('email_confirm').sendKeys(email);
    this.element('next_button').click();
    return true;
  }

});

module.exports = RegistrationForm;
