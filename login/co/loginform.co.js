'use-strict';
/**
 * Created by Reemul Uzamaki 09/16/2016
 */
var Component = require('protractor-pageobject').Component;
var SignInForm = Component.extend({
  els: {
    // email input
    email_ipt:  by.css('#login_form input[type="email"]'),
    email_err: by.id('username_error'),
    password_ipt: by.css('#login_form input[type="password"]'),
    password_err: by.id('password_error'),
    remember_cbx: by.css('#login_form input[type="checkbox"]'),
    remember_lbl: by.xpath('//*[@id="login_form"]/section[3]/div[1]/label'),
    // TODO : show password button
    // TODO : remeber me checkbox
    // sign in button
    signin_btn: by.css('#login_form input[type="submit"]')
  },
  comps: {

  },
  emailInput: function(){
    return {
      isDisplayed: this.element('email_ipt').isDisplayed(),
      placeHolder: this.element('email_ipt').getAttribute('placeholder')
    }
  },
  emailInputError: function(){
    return {
      isDisplayed: this.element('email_err').isDisplayed()
    }
  },
  passwordInput: function(){
    return {
      isDisplayed: this.element('password_ipt').isDisplayed(),
      placeHolder: this.element('password_ipt').getAttribute('placeholder')
    }
  },
  passwordInputError: function(){
    return {
      isDisplayed: this.element('password_err').isDisplayed()
    }
  },
  rememberMeChkBox: function(){
    return {
      isDisplayed: this.element('remember_cbx').isDisplayed()
    }
  },
  rememberMeChkBoxLabel: function(){
    return {
      isDisplayed: this.element('remember_lbl').isDisplayed(),
      text: this.element('remember_lbl').getText()
    }
  },

  signInBtn: function(){
    return {
      isDisplayed: this.element('signin_btn').isDisplayed()
    }
  },

  login: function(e, p){
    var email = this.element('email_ipt');
    var pwd = this.element('password_ipt');
    email.sendKeys(e);
    pwd.sendKeys(p);
    this.element('signin_btn').click();

    return true;
  }


});
module.exports = SignInForm;
