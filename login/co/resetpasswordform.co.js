'use-strict';
/**
 * Created by Reemul Uzamaki 09/19/2016
 */
var Component = require('protractor-pageobject').Component;

var ResetPasswordForm = Component.extend({
  els: {
    email_input:    by.css('#re_form input[type="email"]'),
    next_button:    by.css('#re_form input[type="submit"]')
  },
  comps: {

  },

  next: function(email){
    //var deferred = protractor.promise.defer();
    this.element('email_input').sendKeys(email);
    this.element('next_button').click();
  }
});

module.exports = ResetPasswordForm;
