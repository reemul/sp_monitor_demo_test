'use-strict';
**
 * Created by Reemul Uzamaki 10/04/2016
 */
var DashboardPO = require('dashboard.po.js');
var util = require('../utils');

describe('Bill Savings update', function(){
  var accounts = require('onboarded.js');
  var account = accounts[0];

      var dashboardPO = new DashboardP();
      var _message_ = 'testing user account: '+account.user+ ' - pass: '+parseInt(i+1);
      describe(_message_, function(){
        afterAll(function(){
          browser.executeScript("localStorage.removeItem('user');");
          browser.executeScript("localStorage.removeItem('ug');");
        });

        beforeAll(function(){
          util.loginUser(account.user, account.password);
          util.waitForPage('dashboard').then(function onSuccess(url){
            expect(url.indexOf('/dashboard') > -1).toEqual(true);
          });
        });

        it('should verify the Home Profile widget exists', function(){
          expect(dashboardPO.billsavings().widget.isDisplayed).toBeTruthy();
        });
    });
});
