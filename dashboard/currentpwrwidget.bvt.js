'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var DashboardPO = require('dashboard.po');
var util = require('../utils');
describe('Current Power Widget', function(){
  var dashboardPO;
  var accounts = require('onboarded.pre.js');
  var TITLE = 'Current Power';
  var HEADING = 'Now Producing';

  describe('Widget element verification: ', function(){
    beforeAll(function(){
      util.loginUser(accounts[0].user, accounts[0].password);
      util.waitForPage('dashboard').then(function(){
        browser.getCurrentUrl().then(
          function onSuccess(url){
            dashboardPO = new DashboardPO();
          }
        );
      });
    });

    afterAll(function(){
        browser.executeScript("localStorage.removeItem('user');");
        browser.executeScript("localStorage.removeItem('ug');");
    });

    it('should verify the System Information Widget is displayed', function(){
      expect(dashboardPO.currentPower().isDisplayed).toBeTruthy();
    });

    it('should display widget title as Current Power', function(){
      expect(dashboardPO.currentPower().widget.title().text).toEqual(TITLE);
    });

    it('should verify widget options button is displayed', function(){
      expect(dashboardPO.currentPower().widget.opts().isDisplayed).toBeTruthy();
    });

    it('should display info button', function(){
      browser.sleep(3000);
      dashboardPO.currentPower().widget.opts().click();
      browser.sleep(3000);
      expect(dashboardPO.currentPower().widget.info().isDisplayed).toBeTruthy();
    });

    it('should display the Gauge header as Now Producing', function(){
      expect(dashboardPO.currentPower().widget.heading().key).toEqual(HEADING);
    });
  });
});
