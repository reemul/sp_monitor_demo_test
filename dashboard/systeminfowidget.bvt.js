'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var DashboardPO = require('dashboard.po');
var util = require('../utils');
describe('System Information Widget', function(){
  var dashboardPO;
  var accounts = require('onboarded.pre.js');
  var TITLE = 'System Information';

  describe('Widget element verification: ', function(){
    beforeAll(function(){
      util.loginUser(accounts[0].user, accounts[0].password);
      util.waitForPage('dashboard').then(function(){
        browser.getCurrentUrl().then(
          function onSuccess(url){
            dashboardPO = new DashboardPO();
          }
        );
      });

    });

    afterAll(function(){
        browser.executeScript("localStorage.removeItem('user');");
        browser.executeScript("localStorage.removeItem('ug');");
    });

    it('should verify the System Information Widget is displayed', function(){
      expect(dashboardPO.systemInfo().isDisplayed).toBeTruthy();
    });

    it('should display widget title as System Information', function(){
      expect(dashboardPO.systemInfo().widget.title().text).toEqual(TITLE);
    });

    it('should verify widget options button is displayed', function(){
      expect(dashboardPO.systemInfo().widget.opts().isDisplayed).toBeTruthy();
    });

    it('should display settings button', function(){
      browser.sleep(3000);
      dashboardPO.systemInfo().widget.opts().click();
      browser.sleep(3000);
      expect(dashboardPO.systemInfo().widget.settings().isDisplayed).toBeTruthy();
    });

    it('should display info button', function(){
      expect(dashboardPO.systemInfo().widget.info().isDisplayed).toBeTruthy();
    });

    it('should display the site Nickname', function(){
      expect(dashboardPO.systemInfo().widget.siteName().isDisplayed).toBeTruthy();
    });

    it('should display the site name label as Nickname', function(){
      expect(dashboardPO.systemInfo().widget.siteName().key).toEqual('Nickname');
    });

    it('should display the site Nickname value as Eleanor Jones', function(){
      expect(dashboardPO.systemInfo().widget.siteName().value).toEqual('Eleanor Jones');
    });

    it('should display the site Street label as Street', function(){
      expect(dashboardPO.systemInfo().widget.street().key).toEqual('Street');
    });

    it('should display the zipcode', function(){
      expect(dashboardPO.systemInfo().widget.zipcode().isDisplayed).toBeTruthy();
    });

    it('should display the zipcode as 10312', function(){
      expect(dashboardPO.systemInfo().widget.zipcode().keyValuePair.value).toEqual('10312');
    });

    it('should should not display dealer name', function(){
      expect(dashboardPO.systemInfo().widget.dealerName().isDisplayed).toBeTruthy();
    });

  });
});
