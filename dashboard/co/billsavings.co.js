'use-strict';
/**
 * Created by Reemul Uzamaki 09/01/2016
 */
var Component = require('protractor-pageobject').Component;
var Header = require('header.co');
var header = new Header('//div[@sp-lte-bill-savings]//h5');
var util = require('utils');
var BillSavingsWidget = Component.extend({
  els: {
    title: by.xpath('//div[@sp-lte-bill-savings]//h5'),
    options: by.xpath('//div[@sp-lte-bill-savings]//div[@id="sp-dots"]'),
    settings: by.xpath('//div[@sp-lte-bill-savings]/sp-widget-container/div[2]/div/div[1]/div[2]/div/span[1]'),
    info: by.xpath('//div[@sp-lte-bill-savings]/sp-widget-container/div[2]/div/div[1]/div[2]/div/span[2]'),
    caption: by.xpath('//div[@sp-lte-bill-savings]//div[@class="sp-dash-cpt-inline-value"]')
  },
  comps: {
    updateForm: require('utilityrateform.co')
  },
  /*
  title: function(){
    browser.wait(5000);
    var _e = this.element('title');
    var _c = this.component('title');
    return {
      cpt: _c
    }
  },
*/

  title: function(){
    var _e = this.element('title');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText()
    }
  },

  opts: function(){
    var _e = this.element('options');
    return {
      e: _e,
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  },
  settings: function(){
    var _e = this.element('settings');
    return {
      isDisplayed: _e.isDisplayed(),
      form: this.component('updateForm'),
      click: function(){
        _e.click();
      }
    }
  },
  info: function(){
    var _e = this.element('info');
    return {
      isDisplayed: _e.isDisplayed(),
      click: util.click(_e)
    }
  },
  caption: function(){
    var _e = this.element('caption');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText()
    }
  }
});
module.exports = BillSavingsWidget;
