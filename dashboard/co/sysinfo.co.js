'use-strict'
/**
 * Created by Reemul Uzamaki 08/19/2016
 */
var Component = require('protractor-pageobject').Component;
var SystemInfoWidget = Component.extend({
  els: {
    // NOTE : try title: by.xpath('//div[@sp-system-info]//h5')
    title: by.xpath('//div[@sp-system-info]/sp-widget-container/div[2]/div/div[1]/h5'),
    options: by.xpath('//div[@sp-system-info]/sp-widget-container//div[@id="sp-dots"]'),
    settings: by.xpath('//*[@id="container"]/main/div[1]/div[2]/article/div/div[5]/div/sp-widget-container/div[2]/div/div[1]/div[2]/div/span[1]'),
    info: by.xpath('//*[@id="container"]/main/div[1]/div[2]/article/div/div[5]/div/sp-widget-container/div[2]/div/div[1]/div[2]/div/span[2]'),
    siteName: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[1]'),
    street: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[2]'),
    cityState: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[3]'),
    zipcode: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[4]'),
    time: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[5]'),
    sunrise: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[6]'),
    sunset: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[7]'),
    dealerName: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[8]'),
    dealerPhone: by.xpath('//div[@sp-system-info]//sp-widget-container/div[2]/div/div[2]/div/div/div/div[9]')
  },
  comps: {
  },

  dealerName: function(){
    var _e = this.element('dealerName');
    return {
      isDisplayed: _e.isDisplayed(),
      keyValuePair: keyValuePair(_e)
    }
  },

  dealerPhone: function(){
    var _e = this.element('dealerPhone');
    return keyValuePair(_e);
  },

  sunset: function(){
    var _e = this.element('sunset');
    return keyValuePair(_e);
  },

  sunrise: function(){
    var _e = this.element('sunrise');
    return keyValuePair(_e);
  },

  zipcode: function(){
    var _e = this.element('zipcode');
    return {
      isDisplayed: _e.isDisplayed(),
      keyValuePair: keyValuePair(_e)
    }
  },

  cityState: function(){
    var _e = this.element('cityState');
    return keyValuePair(_e);
  },

  street: function(){
    var _e = this.element('street');
    return keyValuePair(_e);
  },

  siteName: function(){
    var _e = this.element('siteName');
    return keyValuePair(_e);
  },

  title: function(){
    var _e = this.element('title');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText()
    }
  },

  opts: function(){
    var _e = this.element('options');
    return {
      e: _e,
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  },

  settings: function(){
    var _e = this.element('settings');
    return {
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  },

  info: function(){
    var _e = this.element('info');
    return {
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  }
});

function keyValuePair(_e){
  return {
    isDisplayed: _e.isDisplayed(),
    key: _e.getText().then(function(str){
      var array = str.split(':');
      return array[0].trim();
    }),
    value: _e.getText().then(function(str){
      var array = str.split(':');
      return array[1].trim();
    })
  }
};

module.exports = SystemInfoWidget;
