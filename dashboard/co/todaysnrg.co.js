'use-strict'
/**
 * Created by Reemul Uzamaki 08/22/2016
 */
var Component = require('protractor-pageobject').Component;

var TodaysNrgWidget = Component.extend({
  els: {
    widget: by.id('todays-graph-landscape')
    // TODO : add header component and create header component file
  },

  todaysnrg: function(){
    return this.element('widget').isDisplayed();
  }

});
module.exports = TodaysNrgWidget;
