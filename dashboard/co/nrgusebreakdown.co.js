'use-strict'
/**
 * Created by Reemul Uzamaki 09/01/2016
 */
var Component = require('protractor-pageobject').Component;

var NrgUseBreakdownWidget = Component.extend({
  els: {
    widget: by.xpath('//div[@sp-energy-use]')
  },

  nrgusebreakdown: function(){
    return this.element('widget').isDisplayed();
  }

});
module.exports = NrgUseBreakdownWidget;
