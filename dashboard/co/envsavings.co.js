'use-strict'
/**
 * Created by Reemul Uzamaki 09/01/2016
 */
var Component = require('protractor-pageobject').Component;

var EnvSavingsWidget = Component.extend({
  els: {
    widget: by.xpath('//div[@sp-env-savings]')
  },

  envsavings: function(){
    return this.element('widget').isDisplayed();
  }

});
module.exports = EnvSavingsWidget;
