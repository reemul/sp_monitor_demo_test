'use-strict'
/**
 * Created by Reemul Uzamaki 08/19/2016
 */
var Component = require('protractor-pageobject').Component;

var CurrentPwrWidget = Component.extend({
  els: {
    title: by.xpath('//div[@sp-current-production-gauge]//*/h5'),
    options: by.xpath('//div[@sp-current-production-gauge]//*[@id="sp-dots"]'),
    info: by.xpath('//div[@sp-current-production-gauge]/sp-widget-container/div[2]/div/div[1]/div[2]/div/span'),
    heading: by.xpath('//div[@sp-current-production-gauge]/sp-widget-container/div[2]/div/div[2]/div/div[1]')
  },

  title: function(){
    var _e = this.element('title');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText()
    }
  },
  opts: function(){
    var _e = this.element('options');
    return {
      e: _e,
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  },
  info: function(){
    var _e = this.element('info');
    return {
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  },
  heading: function(){
    var _e = this.element('heading');
    return keyValuePair(_e,'Now Producing')
  }
});

function keyValuePair(_e, label){
  return {
    isDisplayed: _e.isDisplayed(),
    key: _e.getText().then(function(str){
      var k = str.slice(0, label.length);
      return k.trim();
    }),
    value: _e.getText().then(function(str){
      var v = str.split(label);
      return v[0].trim();
    })
  }
};
module.exports = CurrentPwrWidget;
