'use-strict'
/**
 * Created by Reemul Uzamaki 09/01/2016
 */
var Component = require('protractor-pageobject').Component;

var EnergyMixWidget = Component.extend({
  els: {
    title: by.xpath('//*[@id="todays-graph-landscape"]//div/h5'),
    options: by.xpath('//*[@id="todays-graph-landscape"]//div[@id="sp-dots"]'),
    info: by.xpath('//*[@id="todays-graph-landscape"]/sp-widget-container/div[2]/div/div[1]/div[2]/div/span')
  },

  title: function(){
    var _e = this.element('title');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText()
    }
  },

  opts: function(){
    var _e = this.element('options');
    return {
      e: _e,
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  },

  info: function(){
    var _e = this.element('info');
    return {
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  }

});
module.exports = EnergyMixWidget;
