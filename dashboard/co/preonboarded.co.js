'use-strict';
/**
 * Created by Reemul Uzamaki 09/08/2016
 */

var TendrilPreOnboardedCO = function(){
  var util              = require('../../utils'),
      sp_pre_onboarded  = element(by.xpath('//div[@sp-pre-onboarded-widget]')),
      // protractor is returning multiple references to sp-energy-use.header
      header_container  = sp_pre_onboarded.element(by.css('.sp-widget-header-container'));

  this.preonboarded = function(){
    util.waitForElement(sp_pre_onboarded);
    return sp_pre_onboarded.isDisplayed();
  };
};

module.exports = TendrilPreOnboardedCO;
