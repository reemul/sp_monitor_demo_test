'use-strict';
/**
 * Created by Reemul Uzamaki 09/01/2016
 */
var Component = require('protractor-pageobject').Component;
var UtilityAndRatePlanForm = Component.extend({
  els: {
    title: by.xpath('/html/body/div[4]//div/h4'),
    utilityInput: by.xpath('//select[@name="utilities"]'),
    ratePlanInput: by.xpath('//select[@name="rate_plans"]'),
    submit: by.xpath('//input[@type="submit"]')
  },
  comps: {
  },

  title: function(){
    var _e = this.element('title');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText()
    }
  },

  utility: function(){
    var _e = this.element('utilityInput');
    return {
      isDisplayed: _e.isDisplayed(),
      select: function(){
        var num = getRandomInt(1, 6);
        return selectOption(_e, num);
      }
    }
  },
/*
  ratePlan: function(){

  },
*/
  submit: function(){
    var _e = this.element('submit');
    return {
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  }
});
module.exports = UtilityAndRatePlanForm;

function selectOption(e, num){
  var _e = e;
  _e[num].click();
};

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};
