'use-strict';
/**
 * Created by Reemul Uzamaki 09/01/2016
 */
var Component = require('protractor-pageobject').Component;
var util = require('utils');
function HeaderFactory(path){
  var Header = Component.extend({
    els: {
      title: by.xpath(path)
    },
    comps: {},

    title: function(){
      var _e = this.element('title');
      return {
        isDisplayed: _e.isDisplayed(),
        text: _e.getText()
      }
    },
    opts: function(){
      var _e = this.element('options');
      return {
        isDisplayed: _e.isDisplayed(),
        click: function(){
          _e.click();
        }
      }
    },
    settings: function(){
      var _e = this.element('settings');
      return {
        isDisplayed: _e.isDisplayed(),
        click: util.click(_e)
      }
    },
    info: function(){
      var _e = this.element('info');
      return {
        isDisplayed: _e.isDisplayed(),
        click: util.click(_e)
      }
    },
    caption: function(){
      var _e = this.element('caption');
      return {
        isDisplayed: _e.isDisplayed(),
        text: _e.getText()
      }
    }
  });
  return Header;
};


module.exports = HeaderFactory;
