'use-strict';
/**
 * Created by Reemul Uzamaki 10/19/2016
 */
var DashboardPO = require('dashboard.po');
var util = require('../utils');
describe('Lifetime Bill Savings Widget', function(){
  var dashboardPO;
  var accounts = require('accounts.js');
  var TITLE = 'Lifetime Bill Savings Estimate';

  describe('Widget element verification: ', function(){
    beforeAll(function(){
      util.loginUser(accounts.SPD[0].user, accounts.SPD[0].password);
      util.waitForPage('dashboard').then(function(){
        browser.getCurrentUrl().then(
          function onSuccess(url){
            dashboardPO = new DashboardPO();
          }
        );
      });
    });

    afterAll(function(){
        browser.executeScript("localStorage.removeItem('user');");
        browser.executeScript("localStorage.removeItem('ug');");
    });

    it('should verify the Lifetime Bill Savings Widget is displayed', function(){
      expect(dashboardPO.billSavings().isDisplayed).toBeTruthy();
    });

    it('should display widget title as Lifetime Bill Savings Estimate', function(){
      expect(dashboardPO.billSavings().widget.title().text).toEqual(TITLE);
    });

    it('should verify widget options button is displayed', function(){
      expect(dashboardPO.billSavings().widget.opts().isDisplayed).toBeTruthy();
    });

    it('should display info button', function(){
      browser.sleep(3000);
      dashboardPO.billSavings().widget.opts().click();
      browser.sleep(3000);
      expect(dashboardPO.billSavings().widget.info().isDisplayed).toBeTruthy();
    });
  });
});
