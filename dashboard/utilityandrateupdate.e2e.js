'use-strict';
/**
 * Created by Reemul Uzamaki 10/19/2016
 */
var DashboardPO = require('dashboard.po');
var util = require('../utils');
describe('Lifetime Bill Savings Widget', function(){
  var dashboardPO;
  var accounts = require('accounts.js');
  var TITLE = 'Select your utility and rate plan';

  describe('Utility and Rate plan update: ', function(){
    beforeAll(function(){
      util.loginUser(accounts.SPD[0].user, accounts.SPD[0].password);
      util.waitForPage('dashboard').then(function(){
        browser.getCurrentUrl().then(
          function onSuccess(url){
            dashboardPO = new DashboardPO();
          }
        );
      });
    });

    afterAll(function(){
        browser.executeScript("localStorage.removeItem('user');");
        browser.executeScript("localStorage.removeItem('ug');");
    });

    it('should verify the Lifetime Bill Savings Widget is displayed', function(){
      expect(dashboardPO.billSavings().isDisplayed).toBeTruthy();
    });

    it('should display settings button after clicking options button', function(){
      browser.sleep(5000);
      browser.executeScript('window.scrollTo(0,document.body.scrollHeight);');
      dashboardPO.billSavings().widget.opts().click();
      browser.sleep(5000);
      expect(dashboardPO.billSavings().widget.settings().isDisplayed).toBeTruthy();
    });
/*
    it('should display the Utility and Rate plan form after clicking the settings button', function(){
      dashboardPO.billSavings().widget.settings().click();
      browser.sleep(3000);

      expect(dashboardPO.billSavings().widget.settings().form.title.isDisplayed).toBeTruthy();
    });
*/
  });
});
