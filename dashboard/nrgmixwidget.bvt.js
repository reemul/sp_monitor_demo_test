'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var DashboardPO = require('dashboard.po');
var util = require('../utils');
describe('Energy Mix Widget', function(){
  var dashboardPO;
  var accounts = require('onboarded.pre.js');
  var TITLE = 'Today"s Energy';

  describe('Widget element verification: ', function(){
    beforeAll(function(){
      util.loginUser(accounts[0].user, accounts[0].password);
      util.waitForPage('dashboard').then(function(){
        browser.getCurrentUrl().then(
          function onSuccess(url){
            dashboardPO = new DashboardPO();
          }
        );
      });

    });

    afterAll(function(){
        browser.executeScript("localStorage.removeItem('user');");
        browser.executeScript("localStorage.removeItem('ug');");
    });

    it('should verify the Energy Mix Widget is displayed', function(){
      expect(dashboardPO.nrgMix().isDisplayed).toBeTruthy();
    });

    it('should display widget title as Today"s Energy', function(){
      expect(dashboardPO.nrgMix().widget.title().text).toEqual(TITLE);
    });

    it('should verify widget options button is displayed', function(){
      expect(dashboardPO.nrgMix().widget.opts().isDisplayed).toBeTruthy();
    });

    it('should display info button', function(){
      browser.sleep(3000);
      dashboardPO.nrgMix().widget.opts().click();
      browser.sleep(3000);
      expect(dashboardPO.nrgMix().widget.info().isDisplayed).toBeTruthy();
    });
  });
});
