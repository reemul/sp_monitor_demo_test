'use-strict';
/**
 * Created by Reemul Uzamaki 08/19/2016
 */
var Page = require('protractor-pageobject').Page;

var DashboardPage = Page.extend({
  els: {
    systemInfo: by.xpath('//div[@sp-system-info]'),
    currentPwr: by.xpath('//div[@sp-current-production-gauge]'),
    todaysNrg:  by.id('todays-graph-landscape'),
    nrgMix: by.id('energy_mix_landscape'),
    envSavings: by.xpath('//div[@sp-env-savings]'),
    billSavings: by.xpath('//div[@sp-lte-bill-savings]'),
    nrgUseBreakdown: by.xpath('//div[@sp-energy-use]'),
    preOnboarded: by.xpath('//div[@sp-pre-onboarded-widget]')
  },
  comps: {
    systemInfo: require('sysinfo.co'),
    currentPwr: require('currentpwr.co'),
    billSavings: require('billsavings.co')
  },

  preOnboarded: function(){
    this.waitForElement('preOnboarded', 6000);
    var _e = this.element('preOnboarded');
  //  var _w = this.component('preOnboarded');
    return {
      //widget: _w,
      isDisplayed: _e.isDisplayed()
    }
  },

  nrgUseBreakdown: function(){
    this.waitForElement('nrgUseBreakdown', 6000);
    var _e = this.element('nrgUseBreakdown');
    return {
      isDisplayed: _e.isDisplayed()
    }
  },

  lteBillSavings: function(){
    this.waitForElement('billSavings', 6000);
    var _e = this.element('billSavings');
    var _w = this.component('billSavings');
    return {
      widget: _w,
      isDisplayed: _e.isDisplayed()
    }
  },

  lteEnvSavings: function(){
    this.waitForElement('envSavings', 3000);
    var _e = this.element('envSavings');
    //var _w = this.component('envSavings');
    return {
      //widget: _w,
      isDisplayed: _e.isDisplayed()
    }
  },

  todaysNrgMix: function(){
    this.waitForElement('nrgMix', 3000);
    var _e = this.element('nrgMix');
    var _w = this.component('nrgMix');
    return {
      widget: _w,
      isDisplayed: _e.isDisplayed()
    }
  },

  todaysNrg: function(){
    this.waitForElement('todaysNrg', 3000);
    var _e = this.element('todaysNrg');
    return {
      isDisplayed: _e.isDisplayed()
    }
  },

  currentPower: function(){
    this.waitForElement('currentPwr', 3000);
    var _e = this.element('currentPwr');
    var _w = this.component('currentPwr');
    return {
      widget: _w,
      isDisplayed: _e.isDisplayed()
    }
  },
});
module.exports = DashboardPage;
