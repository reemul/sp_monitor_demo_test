'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var DashboardPO = require('dashboard.po');
var util = require('../utils');
describe('Energy Link Dashboard page', function(){
  var dashboardPO;
  //var accounts = require('accounts.js');
  // Insert account.forEach here
  //accounts.tendril.forEach(function(currentUser, index){
    describe('Testing dashboard for Demo account: ', function(){
        beforeAll(function(){
          util.demoLogin();
          browser.sleep(2000);
          dashboardPO = new DashboardPO();
        });

        afterAll(function(){
            browser.executeScript("localStorage.removeItem('user');");
            browser.executeScript("localStorage.removeItem('ug');");
        });

        it('should verify the Dashboard page url', function(){
          util.waitForPage('dashboard').then(function(){
            browser.getCurrentUrl().then(
                function onSuccess(url){
                    expect(url.indexOf('/dashboard') > -1).toEqual(true);
                }
            );
          });
        });

        it('should display Current Production Gauge widget', function(){
          expect(dashboardPO.currentPower().isDisplayed).toBeTruthy();
        });

        it('should display Todays Energy Graph widget', function(){
          expect(dashboardPO.todaysNrg().isDisplayed).toBeTruthy();
        });

        it('should display the Tendril Pre-Onboarded widget', function(){
          expect(dashboardPO.preOnboarded().isDisplayed).toBeTruthy();
        });

        it('should display the Tendril Pre-Onboarded widget', function(){
          expect(dashboardPO.todaysNrgMix().isDisplayed).toBeTruthy();
        });

        it('should display Lifetime Environment Savings widget', function(){
          expect(dashboardPO.lteEnvSavings().isDisplayed).toBeTruthy();
        });

        it('should display the Lifetime Bill Savings widget', function(){
          expect(dashboardPO.lteBillSavings().isDisplayed).toBeTruthy();
        });

/*  TODO : rework these tests to iplement new page/component object pattern
        it('it should display dashboard navigation Menu', function(){
          expect(dashboardPO.dashboardMenu()).toBeTruthy();
        });

        it('it should verify menu button link to Graphs page', function(){
          dashboardPO.goToGraphsPage();
          util.waitForPage('graphs').then(function(){
            browser.getCurrentUrl().then(
                function onSuccess(url){
                    expect(url.indexOf('/graphs') > -1).toEqual(true);
                }
            );
          });
        });

        it('it should verify menu button link to Support page', function(){
          dashboardPO.goToSupportPage();
          util.waitForPage('support').then(function(){
            browser.getCurrentUrl().then(
                function onSuccess(url){
                    expect(url.indexOf('/support') > -1).toEqual(true);
                }
            );
          });
        });

        it('it should verify menu button link to Settings page', function(){
          dashboardPO.goToSettingsPage();
          util.waitForPage('settings').then(function(){
            browser.getCurrentUrl().then(
                function onSuccess(url){
                    expect(url.indexOf('/settings') > -1).toEqual(true);
                }
            );
          });
        });
      */
    }); // end describe
}); // end describe
