'use-strict';
/**
 * Created by Reemul Uzamaki 07/28/2016
 */
var DashboardPO = require('dashboard.po.js');
var SettingsPO = require('settings.po.js');
var util = require('../utils');
var dashboardPO;
var settingsPO;
var MY_ACCOUNT = 'My Account';
var MY_REPORTS = 'My Reports';
var HOME_PROFILE = 'Home Profile';
describe('Setting page validation ', function(){
  //var accounts = require('../eis/specs/accounts.js');

  describe('Verification of Settings page elements and components.', function(){
    afterAll(function(){
      browser.executeScript("localStorage.removeItem('user');");
      browser.executeScript("localStorage.removeItem('ug');");
    });

    beforeAll(function(){
      dashboardPO = new DashboardPO();
      var login = {user:"eis7@test.com", password:"eispwd7"};
      util.loginUser(login.user, login.password);
      util.waitForPage('dashboard').then(function(){
        browser.getCurrentUrl().then(function onSuccess(url){
          util.goToPage('settings');
          util.waitForPage('settings').then(function(){
            browser.getCurrentUrl().then(function onSuccess(url){
              settingsPO = new SettingsPO();
              expect(url.indexOf('/settings') > -1).toEqual(true);
            });
          });
        });
      });
    });

    it('should verify abstract header object title', function(){
      expect(settingsPO.accountInfo().header().title().text).isDisplayed();
    });

    it('should verify that the Account info widget successfully loaded', function(){
      expect(settingsPO.accountInfo().isDisplayed).toBeTruthy();
    });

    it('should verify that the My Reports widget successfully loaded', function(){
      expect(settingsPO.myReports().isDisplayed).toBeTruthy();
    });

    it('should verify that the Home Profile widget successfully loaded', function(){
      expect(settingsPO.homeProfile().isDisplayed).toBeTruthy();
    });

  }); // end describe
});// end describe
