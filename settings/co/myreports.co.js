'use-strict';
/**
 * Created by Reemul Uzamaki 09/21/2016
 */
var Component = require('protractor-pageobject').Component;
var MyReports = Component.extend({
  els: {
    title: by.xpath('//*[@id="sp-my-reports"]/sp-widget-container/div[2]/div/div[1]/h5'),
    options: by.xpath('//*[@id="sp-my-reports"]/sp-widget-container/div[2]/div/div[2]'),
    settings: by.xpath('//*[@id="sp-my-reports"]/sp-widget-container/div[2]/div/div[1]/div[2]/div/a'),
    info: by.xpath('//*[@id="sp-my-reports"]/sp-widget-container/div[2]/div/div[1]/div[2]/div/span')
  },
  comps: {
  },

  title: function(){
    var _e = this.element('title');
    return {
      isDisplayed: _e.isDisplayed(),
      text: _e.getText()
    }
  },
  opts: function(){
    var _e = this.element('options');
    return {
      e: _e,
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  },
  settings: function(){
    var _e = this.element('settings');
    return {
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  },
  info: function(){
    var _e = this.element('info');
    return {
      isDisplayed: _e.isDisplayed(),
      click: function(){
        _e.click();
      }
    }
  }
});
module.exports = MyReports;
