'use-strict';
/**
 * Created by Reemul Uzamaki 09/21/2016
 */
var Page = require('protractor-pageobject').Page;
var Component = require('protractor-pageobject').Component;
var SettingsPO = Page.extend({
  els:{
    header: by.xpath('//header/h3'),
    accountInfo: by.id('sp-my-account'),
    myReports: by.id('sp-my-reports'),
    homeProfile: by.id('sp-home-energy'),
    profileDisabled: by.id('home-energy-disabled')
  },

  comps:{
    accountInfo: require('accountinfo.co'),
    myReports: require('myreports.co'),
    homeProfile: require('homeprofile.co')
  },

  accountInfo: function(){
    this.waitForElement('accountInfo', 3000);
    var _e = this.element('accountInfo');
    var _w = this.component('accountInfo');
    return {
      widget: _w,
      isDisplayed: _e.isDisplayed()
    }
  },

  myReports: function(){
    this.waitForElement('myReports', 3000);
    var _e = this.element('myReports');
    var _w = this.component('myReports');
    return {
      widget: _w,
      isDisplayed: _e.isDisplayed()
    }
  },
  homeProfile: function(){
    this.waitForElement('homeProfile', 3000);
    var _e = this.element('homeProfile');
    var _w = this.component('homeProfile');
    return {
      widget: _w,
      isDisplayed: _e.isDisplayed()
    }
  },
});
module.exports = SettingsPO;
