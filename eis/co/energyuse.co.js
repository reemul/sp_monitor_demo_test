'use-strict';
/**
 * Created by Reemul Uzamaki 07/19/2016
 */

var TendrilEnergyUseCO = function(){
  var util              = require('../../utils'),
      sp_energy_use     = element(by.xpath('//div[@sp-energy-use]')),
      // protractor is returning multiple references to sp-energy-use.header
      header_container  = sp_energy_use.element(by.css('.sp-widget-header-container'));

  this.systemEnergyUse = function(){
    util.waitForElement(sp_energy_use);
    return sp_energy_use.isDisplayed();
  };
};

module.exports = TendrilEnergyUseCO;
