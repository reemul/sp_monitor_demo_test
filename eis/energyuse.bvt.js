'use-strict';
/**
 * Created by Reemul Uzamaki 07/19/2016
 */
var DashBoard = require('dashboard.po.js');
var util = require('../utils');

describe('Tendril Energy Use widget performance test', function(){
  var users = require('onboarded.js');

  users.forEach(function(currentUser, index){

    var i = index+1;
    var dashboardPO = new DashBoard();
    var _message_ = 'user login attempt: '+i+' user: '+currentUser.user;
    describe(_message_, function(){

      afterAll(function(){
        browser.executeScript("localStorage.removeItem('user');");
        browser.executeScript("localStorage.removeItem('ug');");
      });

      it('should validate user reached the dashboard', function(){
        util.loginUser(currentUser.user, currentUser.password);
        util.waitForPage('dashboard').then(function(){
          browser.getCurrentUrl().then(function onSuccess(url){
            expect(url.indexOf('/dashboard') > -1).toEqual(true);
          });
        });
      });

      it('should verify loading of Tendril Energy Use Breakdown widget', function(){
        expect(dashboardPO.nrgUseBreakdown().isDisplayed).toBeTruthy();
      });

      it('should logout user', function(){
        util.signout();
        browser.getCurrentUrl().then(function onSuccess(url){
          expect(url.indexOf('/signout'));
        });
      });


    }); //end describe
  }); //end forEach
}); // end Tendril EIS
