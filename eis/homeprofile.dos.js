'use-strict';
/**
 * Created by Reemul Uzamaki 07/28/2016
 */
var DashboardPO = require('dashboard.po.js');
var SettingsPO = require('settings.po.js');
var util = require('../utils');

describe('Tendril EIS DOS Loaded', function(){
  var accounts = require('onboarded.js');
  // Insert account.forEach here
  accounts.forEach(function(currentUser, index){
    var passes = [{first:'0'}, {second:'1'}];
    passes.forEach(function(pass, i){

      var dashboardPO = new DashboardPO();
      var settingsPO;
      var _message_ = 'testing user account: '+currentUser.user+ ' - pass: '+parseInt(i+1);
      describe(_message_, function(){
        afterAll(function(){
          browser.executeScript("localStorage.removeItem('user');");
          browser.executeScript("localStorage.removeItem('ug');");
        });

        beforeAll(function(){
          util.loginUser(currentUser.user, currentUser.password);
          util.waitForPage('dashboard').then(function(){
            browser.getCurrentUrl().then(function onSuccess(url){
              util.goToPage('settings');
              util.waitForPage('settings').then(function(){
                browser.getCurrentUrl().then(function onSuccess(url){
                  settingsPO = new SettingsPO();
                  expect(url.indexOf('/settings') > -1).toEqual(true);
                });
              });
            });
          });
        });

        it('should verify the Home Profile widget successfully loaded', function(){
          expect(settingsPO.verifyHomeProfile()).toBeTruthy();
        });

        it('should toggle the Profile widget header buttons', function(){
          expect(settingsPO.toggleProfileOptions()).toBeTruthy();
        });

        it('should display the Home Profile Settings modal', function(){
          expect(settingsPO.showHomeProfileSettings()).toBeTruthy();
        });

        it('should display the Tendril Home Profile iframe', function(){
          expect(settingsPO.verifyHomeProfileIFrame()).toBeTruthy();
        });

        it('should display the Tendril Home Profile form', function(){
          expect(settingsPO.verifyHomeProfileForm()).toBeTruthy();
        });

        it('should modify Tendril Home Profile household sqare foot slider', function(){
          var reset = i;
          expect(settingsPO.increaseHouseholdSquareFt(reset)).toBeTruthy();
        });

        it('should submit profile changes to the backend for processing', function(){
          expect(settingsPO.updateProfileChanges()).toBeTruthy();
        });
      }); // end describe
    }); // forEach loop
  }); // forEach loop
});// end describe
