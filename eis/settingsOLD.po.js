'use-strict';
/**
 * Created by Reemul Uzamaki 07/28/2016
 * TODO : This is a depricated page object still connected to some tests
 * See page object in /po directory for new page objects 
 */
var SettingsPO = function(){

  var util                        = require('../utils'),
      // Angular elements
      home_profile_widget         = element(by.id('sp-home-energy')),
      home_profile_disabled       = element(by.id('home-energy-disabled')),
      hp_settings_button          = home_profile_widget.element(by.xpath('sp-widget-container[1]/div/div/div[1]/div[2]/div/span[1]')),
      home_energy_dots            = home_profile_widget.element(by.id('sp-dots')),
      home_profile_settings       = element(by.tagName('sp-home-profile')),
      eis_iframe                  = home_profile_settings.element(by.id('home-profile'));
  // Non-Angular elements
  var homeStats;

  this.isHomeProfileDisabled = function(){
    browser.sleep(3000);
    util.waitForElement(home_profile_widget);
    if(home_profile_widget == 'undefined'){
      util.waitForElement(home_profile_disabled);
      if(home_profile_disabled != 'undefined'){
        return home_profile_disabled.isDisplayed();
      }
    }
    return false;
  };

  // Home Profile widget check
  this.verifyHomeProfile = function(){
    browser.sleep(3000);
    util.waitForElement(home_profile_widget);
    return home_profile_widget.isDisplayed();
  };
/*
  this.toggleProfileOptions = function(){
    var header = new HeaderObj(home_profile_widget);
    util.waitForElement(header._options);
    header._options.click();
    util.waitForElement(header._settings);
    return header._settings.isdisplayed();
  };
*/

  // Toggle profile options check
  this.toggleProfileOptions = function(){
    util.waitForElement(home_energy_dots);
    home_energy_dots.click();
    util.waitForElement(hp_settings_button);
    return hp_settings_button.isDisplayed();
  };

  // Show Home Profile settins check
  this.showHomeProfileSettings = function(){
    if(hp_settings_button){
      hp_settings_button.click();
      util.waitForElement(home_profile_settings);
      browser.sleep(2000); // TODO : refactor util.waitForElement
      if(home_profile_settings){
        return home_profile_settings.isDisplayed();
      }
      return false;
    }
  };

  // Home Profile iFrame check
  //var msg = 'EIS iFrame failed to load';
  this.verifyHomeProfileIFrame = function(){
    if(home_profile_settings){
      util.waitForElement(eis_iframe);
      if(eis_iframe){
        return eis_iframe.isDisplayed();
      }
      return false;
    }
  };

  // Home Profile form check
  this.verifyHomeProfileForm = function(){
    if(eis_iframe){
      browser.sleep(2000);
      browser.driver.switchTo().frame('home-profile');
      homeStats = browser.findElement(by.id('home-stats'));
      if(homeStats){
        return true;
      }
      return false;
    }
  };

  // Modify Household sqare ft.
  this.increaseHouseholdSquareFt = function(reset){
    if(homeStats){
      var handle = homeStats.findElement(by.className('min-slider-handle'));
      var value = homeStats.findElement(by.className('range')).getAttribute('value')
        .then(function(n){ return parseFloat(n); });

        return  handle.getLocation().then(function(location){
          var initX;
          var posX = location.x;
          if(reset == 0){
            var result = moveSlider(posX, handle);
            return result;
          }
          if(reset == 1){
            if(posX != initX){
              var result = moveSlider(posX, handle);
              return result;
            }
          }
        });
      };
  };

  // Update Home Profile changes
  this.updateProfileChanges = function(){
    var update_button = browser.findElement(by.id('home-profile-update'));
    if(update_button){
      update_button.click();
      browser.sleep(2000);
      return true;
    }
    return false;
  };

}; //end DashboardPO

var moveSlider = function(posX, handle){
  if(posX == 270){
    var move = resetSlider(handle);
    return move;
  }
  if(posX < 270){
    var move = slideRight(handle);
    return move;
  }
};
var resetSlider = function(handle){
  browser.actions().mouseDown(handle).perform();
  browser.actions().mouseMove({x:-245, y:0}).perform();
  browser.actions().mouseUp(handle).perform();
  return true;
};
var slideRight = function(handle){
  browser.actions().mouseDown(handle).perform();
  browser.actions().mouseMove({x:49, y:0}).perform();
  browser.actions().mouseUp(handle).perform();
  return true;
};
var slideLeft = function(handle){
  browser.actions().mouseDown(handle).perform();
  browser.actions().mouseMove({x:-49, y:0}).perform();
  browser.actions().mouseUp(handle).perform();
  return true;
};
module.exports = SettingsPO;
