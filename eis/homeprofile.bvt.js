'use-strict';
/**
* Created by Reemul Uzamaki 07/26/2016
*/
var DashBoard = require('dashboard.po.js');
var util = require('../utils');

describe('Energy Home Profile widget verification test', function(){
  var users = require('onboarded.pre.js');

  users.forEach(function(currentUser, index){
    var i = index+1;
    var dashboardPO = new DashBoard();
    var _message_ = 'user energy profile account: '+currentUser.user;
    describe(_message_, function(){

      afterAll(function(){
        browser.executeScript("localStorage.removeItem('user');");
        browser.executeScript("localStorage.removeItem('ug');");
      });

      it('should validate user reached the dashboard', function(){
        util.loginUser(currentUser.user, currentUser.password);
        util.waitForPage('dashboard').then(function(){
          browser.getCurrentUrl().then(function onSuccess(url){
            expect(url.indexOf('/dashboard') > -1).toEqual(true);
          });
        });
      });

      it('should verify loading of Tendril EIS Home Profile widget', function(){
        expect(dashboardPO.preOnboarded().isDisplayed).toBeTruthy();
      });

      it('should logout user', function(){
        util.signout();
        browser.getCurrentUrl().then(function onSuccess(url){
          expect(url.indexOf('/signout'));
        });
      });
    });// end describe
  }); // end forEach
}); // end describe
