'use-strict'
/**
  * Created by Reemul Uzamaki 08/26/2016
  */
module.exports = [
  { user: "eis6@test.com", password: "eispwd6" },
  { user: "eis7@test.com", password: "eispwd7" },
  { user: "eis8@test.com", password: "eispwd8" },
  { user: "eis9@test.com", password: "eispwd9" },
  { user: "eis10@test.com", password: "eispwd10" }
];
