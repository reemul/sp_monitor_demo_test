var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['./dashboard/dashboard.bvt.js'])
      .pipe(protractor({
        configFile: './conf.bvt.js'
      }));
  }
};
