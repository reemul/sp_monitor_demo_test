var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['login/*.e2e.js', 'dashboard/*.e2e.js', 'eis/*.e2e.js',  'settings/*.e2e.js'])
      .pipe(protractor({
        configFile: './conf.e2e.js'
      }));
  }
};
