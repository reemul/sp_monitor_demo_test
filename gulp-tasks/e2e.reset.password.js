'use-strict';
/**
 * Created by Reemul Uzamaki 09/19/2016
 */
var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['./login/resetpassword.e2e.js'])
      .pipe(protractor({
        configFile: './conf.e2e.js'
      }));
  }
};
