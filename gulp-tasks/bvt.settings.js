
'use-strict';
/**
 * Created by Reemul Uzamaki 09/21/2016
 */
var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['./settings/settings.bvt.js'])
      .pipe(protractor({
        configFile: './conf.bvt.js'
      }));
  }
};
