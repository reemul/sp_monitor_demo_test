var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['login/*.bvt.js', 'dashboard/*.bvt.js', 'eis/*.bvt.js',  'settings/*.bvt.js'])
      .pipe(protractor({
        configFile: './conf.bvt.js'
      }));
  }
};
