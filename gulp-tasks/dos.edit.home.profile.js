var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['./settings/edithomeprofile.e2e.js'])
      .pipe(protractor({
        configFile: './conf.dos.js'
      }));
  }
};
