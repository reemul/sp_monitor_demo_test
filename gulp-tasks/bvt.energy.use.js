var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['./eis/specs/energyuse.bvt.js'])
      .pipe(protractor({
        configFile: './conf.bvt.js'
      }));
  }
};
