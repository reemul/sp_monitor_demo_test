# Gulp Tasks

Each Gulp task resides in its own file and is loaded into the *gulpfile.js* by the *gulp-require-tasks* module.

## Task names

Task names are determined by the task file name minus the *.js* extension. If you are using a Gulp plugin such as the *Gulp Manager* for the Atom IDE, the fetch button will retrieve the gulp names for you and run them by which ever taks is clicked. If you are using the the command line, simply type _gulp task.name_ without the *.js* extension.

## Naming conventions

The naming conventions for gulp tests vary according to the testing purpose.

_*.bvt.js_ - bvt denotes Basic Verification Test. These tests are used for testing each page for the presence of elements and components, and matching content to specifications.

_*.dos.js_ - dos denotes Denial of Service or pseudo Stress or performance test. These test general carry out some functionality in a repetitious manner in multiple browsers. These tests are limited by the capabilities of the machine running them.

_*.e2e.js_ - e2e denotes End to End Test. These tests are used for testing particular process flow from the user perspective. 
