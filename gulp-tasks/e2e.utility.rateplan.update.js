var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['./dashboard/utilityandrateupdate.e2e.js'])
      .pipe(protractor({
        configFile: './conf.e2e.js'
      }));
  }
};
