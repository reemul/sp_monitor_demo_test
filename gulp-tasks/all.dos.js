var protractor = require('gulp-protractor').protractor;

module.exports = {
  dep: ['webdriver.update'],
  fn: function(gulp, callback){
    return gulp.src(['login/*.dos.js', 'dashboard/*.dos.js', 'eis/*.dos.js',  'settings/*.dos.js'])
      .pipe(protractor({
        configFile: './conf.dos.js'
      }));
  }
};
