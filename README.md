#**SPA Automation Test Framework**#
This framework is used to test the SunPower Energy Link web application.

###**Environment prerequisites:**###
- **Node.js 4.4.5**
  Install with windows installer. To verify which version of Node.js is installed type:
  `node -v`
- **NPM 2.25.5**
  NPM should have installed with Node.js. To update NPM type:
  `npm install npm -g`
  To check the version of NPM type:
  `npm -v`
- **Gulp-cli**
  To install Gulp-cli type:
  `npm install -g gulp-cli`

###**Project dependencies***###
The package.json file contains the remaining required packages which can be installed using
NPM. The following commands will install the remaining packages.
`npm install -g node-gyp
 npm install`
