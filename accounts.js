'use-strict'
/**
  * Created by Reemul Uzamaki 08/26/2016
  */
module.exports = {
  tendril: [
    { user: "eis11@test.com", password: "eispwd11" },
    { user: "eis12@test.com", password: "eispwd12" },
    { user: "eis13@test.com", password: "eispwd13" },
    { user: "eis14@test.com", password: "eispwd14" },
    { user: "eis15@test.com", password: "eispwd15" }
  ],
  SPD: [
    { user: "spdtest1@test.com", password: "sunshine12" },
    { user: "spdtest2@test.com", password: "sunshine12" },
    { user: "spdtest4@test.com", password: "sunshine12" },
    { user: "spdtest5@test.com", password: "sunshine12" },
    { user: "spdtest6@test.com", password: "sunshine12" }
  ],
  PVS5: [
    { user: "pvs5_test1@yahoo.com", password: "pvs5pwd1"},
    { user: "pvs5_test2@yahoo.com", password: "pvs5pwd2"},
    { user: "pvs5.test3@gmail.com", password: "pvs5pwd3"},
    { user: "pvs5_test4@outlook.com", password: "pvs5pwd4"}
  ],
  alerts: [
    { user: "ramatest1@test.com", password: "sun"},
    { user: "ramatest1@test.com", password: "M1ruthi1"},
    { user: "icdnm2@test.com", password: "sun"},
    { user: "ramatest5@test.com", password: "sun"},
    { user: "icedealer@test.com", password: "icedealer1"}
  ],
  conEdison: [
    { user: "ces1_test@yahoo.com", password: "cespwd1" },
    { user: "ces2_test@yahoo.com", password: "cespwd2" },
    { user: "ces3@test.com", password: "cespwd3" },
    { user: "ces4@test.com", password: "cespwd4" }
  ]
};
