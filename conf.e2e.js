'use-strict';
/**
 * Created by Reemul Uzamaki 07/05/2016
 */
var _po_dir = './po/';
/* NOTE : HtmlScreenshotReporter turned off
var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter-2');
var reporterV2 = new HtmlScreenshotReporter({
   dest: 'reportsV2/screenshots/eis_dos_tmp',
   filename: 'report.html',
   cleanDestination: true,
   showSummary: false,
   showConfiguration: false,
   reportTitle: null,
   preserveDirectory: false,
   userCss: 'reportstyles.css'
 });
*/
exports.config = {
  seleniumServerJar: '../../node_modules/protractor/selenium-server-standalone-2.47.1.jar',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  keepAlive: false,

  multiCapabilities: [{
    browserName: 'chrome',
    count: 1,
    specs: ['eis_homeprofile.dos.js']
  }],
  specs: [],
  suites: {
    login: './login/*.bvt.js',
    dashboard: './dashboard/*.bvt.js',
    eis: './eis/specs/*.*.js',
    createAccount:'./create_account/*.spec.js'
  },
/*
  beforeLaunch: function(){
    return new Promise(function(resolve){
      reporterV2.beforeLaunch(resolve);
    });
  },

  afterLaunch: function(exitCode){
    return new Promise(function(resolve){
      reporterV2.afterLaunch(resolve.bind(this, exitCode));
    });
  },
*/
  // end Reports V2 setup
  onPrepare: function(){
      var width = 1200;
      var height = 900;
      browser.driver.manage().window().setSize(width, height);

      // Disable animations so e2e tests run more quickly
/*
      var disableNgAnimate = function() {
          angular.module('disableNgAnimate', []).run(['$animate', function($animate) {
              $animate.enabled(false);
          }]);
      };
      browser.addMockModule('disableNgAnimate', disableNgAnimate);

      var disableSpinner = function(){
          angular.module('disableSpinner', []).config(['spinnerServiceProvider', function(spinnerServiceProvider){
              spinnerServiceProvider.enabled(false);
          }]);
      };
      //browser.addMockModule('disableSpinner', disableSpinner);
*/
      // for non-angular pages
      browser.ignoreSynchronization = true;

      var SpecReporter = require('jasmine-spec-reporter');
      // add jasmine spec reporter
      jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: true}));
      // jasmine.getEnv().addReporter(reporter);
      /* NOTE : HtmlScreenshotReporter turned off
      jasmine.getEnv().addReporter(reporterV2);
      */
  },

  params: {
    globalpaths: require('./globalpaths.js')
  },
  jasmineNodeOpts: {
    onComplete: null,
    defaultTimeInterval: 10000,
    isVerbose: true,
    showColors: true,
    includeStackTrace: true
  }
};
