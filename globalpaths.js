'use-strict';
/**
 * Created by Reemul Uzamaki 09/23/2016
 */
// page object paths
require('app-module-path').addPath('./');
require('app-module-path').addPath('./login/po/');
require('app-module-path').addPath('./dashboard/po/');
require('app-module-path').addPath('./settings/po/');
require('app-module-path').addPath('./settings/');
require('app-module-path').addPath('./eis/po/');
// component object paths
require('app-module-path').addPath('./co');
require('app-module-path').addPath('./login/co/');
require('app-module-path').addPath('./dashboard/co/');
require('app-module-path').addPath('./settings/co/');
require('app-module-path').addPath('./eis/co/');
